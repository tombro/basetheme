<?php

$basetheme_includes = [
  'lib/setup.php',     // Theme setup
  'lib/assets.php',     // Assets
  'lib/functions_common.php',    // Common functions
  'lib/functions_custom.php',    // Custom functions
  'lib/customizer.php', // Theme customizer
  'lib/widgets.php', // Custom Widgets
  'lib/template-tags.php', // Template Tags
  'lib/template-functions.php', // Template Functions
  'lib/bootstrap-navwalker.php' // Bootstrap Walker
];

foreach ($basetheme_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf('Error locating %s for inclusion', $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!-- A little IE support // only IE lt 10 supports conditional comments -->
<!--[if IE]>
	<link href="<?php echo asset_path('/styles/bootstrap-ie8.css'); ?>" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/g/html5shiv@3.7.3"></script>
	<script src="https://cdn.jsdelivr.net/g/respond@1.4.2"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php get_template_part('template-parts/navigation/navigation', 'top'); ?>
	<main role="main">
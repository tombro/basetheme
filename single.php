<?php
get_header(); ?>
<div class="container-fluid">
	<div class="row">
		<div id="primary" class="col-sm-8 content-area">
			<?php
                while (have_posts()) : the_post();

                    get_template_part('template-parts/post/content', get_post_format());

                endwhile;
            ?>
		</div><!-- #primary -->
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer();

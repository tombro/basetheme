<?php get_header(); ?>
<div class="container-fluid">
	<div id="primary" class="row">
		<div class="col-12">
			<?php
            while (have_posts()) : the_post();

                get_template_part('template-parts/page/content', 'page');

            endwhile;
            ?>
		</div>
	</div>
</div>
<?php get_footer();

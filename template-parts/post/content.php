<?php
$id 		= get_the_ID();
$thumbnail 	= get_the_post_thumbnail_url(null);
$title 		= get_the_title();
$link 		= esc_url(get_permalink());
?>

<article id="post-<?php echo $id; ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php echo $title; ?></h1>
		<?php if ('post' === get_post_type()) : ?>
			<div class="entry-meta">
				<?php the_date(); ?>
			</div>
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ($thumbnail) : ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php echo $thumbnail; ?>" alt="<?php echo $title; ?>">
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>

	<div class="entry-content">
		<?php
        the_content(sprintf(
            __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'basetheme'),
            get_the_title()
        ));

        wp_link_pages(array(
            'before'      => '<div class="page-links">' . __('Pages:', 'basetheme'),
            'after'       => '</div>',
            'link_before' => '<span class="page-number">',
            'link_after'  => '</span>',
        ));
        ?>
	</div><!-- .entry-content -->

	<?php if (is_single()) : ?>
		<?php edit_post_link(); ?>
	<?php endif; ?>

</article><!-- #post-## -->

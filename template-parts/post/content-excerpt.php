<?php
    $thumbnail 	= get_the_post_thumbnail_url(null, 'thumbnail');
    $id 		= get_the_ID();
    $title 		= get_the_title();
    $link 		= esc_url(get_permalink());
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
	<?php if ($thumbnail): ?>
		<div class="post-thumbnail col-sm-4">
			<a href="<?php echo $link; ?>">
				<img src="<?php echo $thumbnail; ?>" alt="<?php echo $title; ?>">
			</a>
		</div>
	<?php endif; ?>
	<div class="<?php echo($thumbnail != '' ? 'col-sm-8' : 'col-12'); ?>">
		<header class="entry-header">
			<h2 class="entry-title">
				<a href="<?php echo $link; ?>" rel="bookmark">
					<?php echo $title; ?>
				</a>
			</h2>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php
            the_content(sprintf(
                __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'basetheme'),
                get_the_title()
            ));

            wp_link_pages(array(
                'before'      => '<div class="page-links">' . __('Pages:', 'basetheme'),
                'after'       => '</div>',
                'link_before' => '<span class="page-number">',
                'link_after'  => '</span>',
            ));
            ?>
		</div><!-- .entry-content -->
	</div>
</article><!-- #post-## -->

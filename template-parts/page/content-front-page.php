<?php
    $title = get_the_title();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >

	<div class="panel-content">
		<header class="entry-header">
			<h2 class="entry-title">
				<?php echo $title; ?>
			</h2>
			<?php edit_post_link(); ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php
                /* translators: %s: Name of current post */
                the_content(sprintf(
                    __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'basetheme'),
                    get_the_title()
                ));
            ?>
		</div><!-- .entry-content -->
	</div><!-- .panel-content -->

</article><!-- #post-## -->

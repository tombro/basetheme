<?php
$id 	= get_the_ID();
$title 	= get_the_title();
?>
<article id="post-<?php echo $id; ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php echo $title; ?></h1>
		<?php edit_post_link(); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->

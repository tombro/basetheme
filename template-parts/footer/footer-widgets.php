<?php if (is_active_sidebar('footer')) : ?>

	<aside class="widget-area" role="complementary">
		<div class="widget-column footer-widgets">
			<?php dynamic_sidebar('footer'); ?>
		</div>
	</aside><!-- .widget-area -->

<?php endif; ?>

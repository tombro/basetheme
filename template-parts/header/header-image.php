<?php 
    $logo_id 	= get_theme_mod('custom_logo');
    $logo 		= wp_get_attachment_image_src($logo_id, 'medium');
    $home_link	= esc_url(home_url('/'));
?>
<a class="navbar-brand" href="<?php echo $home_link; ?>">
<?php echo '<img src="'. esc_url($logo[0]) .'">'; ?>
</a>
<?php
/**
 * Displays top navigation
 */

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<?php get_template_part('template-parts/header/header', 'image'); ?>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<?php
        $args = array(
            'theme_location' 	=> 'top',
            'walker' 			=> new Wp_Bootstrap_Navwalker(),
            'menu_id' 			=> 'top-menu',
            'menu_class' 		=> 'navbar-nav',
            'container'			=> false
        );
        wp_nav_menu($args);
        ?>
	</div>
</nav>
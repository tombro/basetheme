		</main>
		<footer id="colophon" class="site-footer container-fluid" role="contentinfo">
			
				<?php
                get_template_part('template-parts/footer/footer', 'widgets');
                get_template_part('template-parts/footer/site', 'info');
                ?>

		</footer><!-- #colophon -->
		<?php wp_footer(); ?>
	</body>
</html>

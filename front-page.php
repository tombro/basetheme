<?php
get_header(); ?>
<div class="container-fluid">
	<div id="primary" class="row">
		<div class="col-12">
			<?php
            if (have_posts()) :
                while (have_posts()) : the_post();
                    get_template_part('template-parts/page/content', 'front-page');
                endwhile;
            else :
                get_template_part('template-parts/post/content', 'none');
            endif; ?>
		</div>
	</div>
</div>
<?php get_footer();

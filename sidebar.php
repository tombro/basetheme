<?php
if (! is_active_sidebar('main')) {
    return;
}
?>
<aside id="secondary" class="col-sm-4 widget-area" role="complementary">
	<?php dynamic_sidebar('main'); ?>
</aside><!-- #secondary -->

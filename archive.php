<?php
/**
 * The template for displaying archive pages
 */

get_header(); ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<?php if (have_posts()) : ?>
				<header class="page-header">
					<?php
                    the_archive_title('<h1 class="page-title">', '</h1>');
                    the_archive_description('<div class="taxonomy-description">', '</div>');
                    ?>
				</header><!-- .page-header -->
			<?php endif; ?>
		</div>
	</div>
	<div class="row">

		<div id="primary" class="col-8 content-area">

			<?php
            if (have_posts()) : ?>
			<?php
            while (have_posts()) : the_post();

                    get_template_part('template-parts/post/content', 'excerpt');

                endwhile;

                the_posts_pagination(array(
                    'prev_text' => '<i class="fa fa-arrow-left" aria-hidden="true"></i><span class="screen-reader-text">' . __('Previous page', 'basetheme') . '</span>',
                    'next_text' => '<span class="screen-reader-text">' . __('Next page', 'basetheme') . '</span><i class="fa fa-arrow-right" aria-hidden="true"></i>',
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Page', 'basetheme') . ' </span>',
                ));

            else :

                get_template_part('template-parts/post/content', 'none');

            endif; ?>

		</div><!-- #primary -->
		<?php get_sidebar(); ?>

	</div>
	
</div>

<?php get_footer();

<?php
get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<?php if (is_home() && ! is_front_page()): ?>
				<header class="page-header">
					<h1 class="page-title"><?php single_post_title(); ?></h1>
				</header>
			<?php else: ?>
				<header class="page-header">
					<h2 class="page-title"><?php _e('Posts', 'basetheme'); ?></h2>
				</header>
			<?php endif; ?>
		</div>
	</div>
	<div class="row">
		<div id="primary" class="col-8 content-area">
			<?php
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    get_template_part('template-parts/post/content', get_post_type());
                }

                the_posts_pagination(array(
                    'prev_text' => '<i class="fa fa-arrow-left" aria-hidden="true"></i><span class="screen-reader-text">' . __('Previous page', 'basetheme') . '</span>',
                    'next_text' => '<span class="screen-reader-text">' . __('Next page', 'basetheme') . '</span><i class="fa fa-arrow-right" aria-hidden="true"></i>',
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Page', 'basetheme') . ' </span>',
                ));
            } else {
                get_template_part('template-parts/post/content', 'none');
            }
            ?>
		</div><!-- #primary -->
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer();

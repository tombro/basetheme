<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 */

/**
 * Flush out the transients used in basetheme_categorized_blog.
 */
function basetheme_category_transient_flusher()
{
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    // Like, beat it. Dig?
    delete_transient('basetheme_categories');
}
add_action('edit_category', 'basetheme_category_transient_flusher');
add_action('save_post', 'basetheme_category_transient_flusher');

<?php

function basetheme_setup()
{
    add_filter('wpseo_metabox_prio', function () {
        return 'low';
    });
    add_filter('use_default_gallery_style', '__return_false');

    // Allow svg uploads
    function basetheme_mime_types($mimes)
    {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }
    add_filter('upload_mimes', 'basetheme_mime_types');

    // Remove certain menu items from admin
    function basetheme_remove_menus()
    {
        remove_menu_page('edit-comments.php');          //Comments
    }
    add_action('admin_menu', 'basetheme_remove_menus');

    /*
     * Make theme available for translation.
     * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/basetheme
     * If you're building a theme based on Basetheme, use a find and replace
     * to change 'basetheme' to the name of your theme in all the template files.
     */
    load_theme_textdomain('basetheme', get_template_directory() . '/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus(array(
        'top'    => __('Main Menu', 'basetheme'),
        'social' => __('Social Links Menu', 'basetheme'),
    ));

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    // Add theme support for Custom Logo.
    add_theme_support('custom-logo', array( ));

    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');

    /*
     * This theme styles the visual editor to resemble the theme style,
     * specifically font, colors, and column width.
     */
    add_editor_style(array( 'assets/css/editor-style.css' ));

    // Disable rest API
    function basetheme_disable_rest_api()
    {
        return new WP_Error('rest_disabled', __('The REST API on this site has been disabled.'), array( 'status' => rest_authorization_required_code() ));
    }
    add_filter('rest_authentication_errors', 'basetheme_disable_rest_api');
}
add_action('after_setup_theme', 'basetheme_setup');

function basetheme_hide_dev_plugins()
{

    // Hide ACF menu item
    $basetheme_acf_pro = get_theme_mod('basetheme_acf_pro', 0);
    if ($basetheme_acf_pro != 1) {
        remove_menu_page('edit.php?post_type=acf-field-group');
    }

    // Hide W3TC menu item
    $basetheme_w3tc = get_theme_mod('basetheme_w3tc', 0);
    if ($basetheme_w3tc != 1) {
        remove_menu_page('w3tc_dashboard');
    }
}
add_action('admin_menu', 'basetheme_hide_dev_plugins', 999);

/**
 * Clean up admin bar
 **/
function basetheme_clean_admin_bar($wp_admin_bar)
{
    $wp_admin_bar->remove_node('wp-logo');
    $wp_admin_bar->remove_node('comments');

    $basetheme_w3tc = get_theme_mod('basetheme_w3tc', 0);
    if ($basetheme_w3tc != 1) {
        $wp_admin_bar->remove_node('w3tc_overlay_upgrade');
        $wp_admin_bar->remove_node('w3tc_settings_extensions');
        $wp_admin_bar->remove_node('w3tc_settings_general');
        $wp_admin_bar->remove_node('w3tc_settings_faq');
        $wp_admin_bar->remove_node('w3tc_support');
    }
}
add_action('admin_bar_menu', 'basetheme_clean_admin_bar', 999);

/**
 * Add preconnect for Google Fonts.
 **/
function basetheme_resource_hints($urls, $relation_type)
{
    if (wp_style_is('basetheme-fonts', 'queue') && 'preconnect' === $relation_type) {
        $urls[] = array(
            'href' => 'https://fonts.gstatic.com',
            'crossorigin',
        );
    }

    return $urls;
}
add_filter('wp_resource_hints', 'basetheme_resource_hints', 10, 2);

/**
 * Register widget area.
 */
function basetheme_widgets_init()
{
    register_sidebar(array(
        'name'          => __('Sidebar', 'basetheme'),
        'id'            => 'main',
        'description'   => __('Add widgets here to appear in your sidebar.', 'basetheme'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ));
    register_sidebar(array(
        'name'          => __('Footer', 'basetheme'),
        'id'            => 'footer',
        'description'   => __('Add widgets here to appear in your footer.', 'basetheme'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ));
}
add_action('widgets_init', 'basetheme_widgets_init');

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 */
function basetheme_excerpt_more($link)
{
    if (is_admin()) {
        return $link;
    }

    $link = sprintf(
        '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
        esc_url(get_permalink(get_the_ID())),
        /* translators: %s: Name of current post */
        sprintf(__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'basetheme'), get_the_title(get_the_ID()))
    );
    return ' &hellip; ' . $link;
}
add_filter('excerpt_more', 'basetheme_excerpt_more');

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 */
function basetheme_javascript_detection()
{
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action('wp_head', 'basetheme_javascript_detection', 0);

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function basetheme_pingback_header()
{
    if (is_singular() && pings_open()) {
        printf('<link rel="pingback" href="%s">' . "\n", get_bloginfo('pingback_url'));
    }
}
add_action('wp_head', 'basetheme_pingback_header');

/**
 * Add google analytics to every page
 */
function basetheme_google_analytics()
{
    if (get_theme_mod('basetheme_ga')) {
        ?>
	  <script>
	    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	    ga('create', '<?php echo get_theme_mod('basetheme_ga') ?>', 'auto');
	    ga('send', 'pageview');
	    ga('set', 'anonymizeIp', true);
	  </script>
	<?php
    }
}
add_action('wp_head', 'basetheme_google_analytics');

/**
 * Enqueue scripts and styles.
 */
function basetheme_enqueue_scripts()
{
    // Theme stylesheet.
    wp_enqueue_style('basetheme-style', asset_path('/styles/main.css'), false, null);

    // Load the html5 shiv.
    wp_enqueue_script('html5', asset_path('/scripts/html5.js'), array(), '3.7.3');
    wp_script_add_data('html5', 'conditional', 'lt IE 9');

    // Skip Focus Link FIX
    wp_enqueue_script('basetheme-skip-link-focus-fix', asset_path('/scripts/skip-link-focus-fix.min.js'), array(), '1.0', true);

    $basetheme_l10n = array(
        'quote' => array( 'icon' => 'quote-right' ),
    );

    wp_localize_script('basetheme-skip-link-focus-fix', 'bastehemeScreenReaderText', $basetheme_l10n);

    // Main stylesheet
    wp_enqueue_script('scripts', asset_path('/scripts/main.js'), array( 'jquery' ), '1.0', true);
}
add_action('wp_enqueue_scripts', 'basetheme_enqueue_scripts');
?>
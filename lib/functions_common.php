<?php

function basetheme_acf_init()
{
    acf_update_setting('google_api_key', 'AIzaSyCqzZn-_BrMtbcC8WQYPMgDWbGm7sTeiBs');
}
add_action('acf/init', 'basetheme_acf_init');

function basetheme_default_settings()
{

    // Images
    update_option('image_default_align', 'none');
    update_option('image_default_link_type', 'file');
    update_option('image_default_link_type', 'file');
    
    // Discussion
    update_option('default_comment_status', '0');
    update_option('default_ping_status', '0');

    update_option('moderation_notify', '0');
    update_option('comments_notify', '0');
}
add_action('after_setup_theme', 'basetheme_default_settings');

global $pagenow;
if ($pagenow == 'options-discussion.php') :

    function basetheme_settings_admin_notice()
    {
        echo '<div class="updated"><p>' . __('Some of these settings are controlled by the theme. To change these, please contact the theme author', 'basetheme') . '</p></div>';
    }
    add_action('admin_notices', 'basetheme_settings_admin_notice');

endif;

function basetheme_unhide_kitchensink($args)
{
    $args['wordpress_adv_hidden'] = false;
    return $args;
}
add_filter('tiny_mce_before_init', 'basetheme_unhide_kitchensink');

remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'rsd_link');

remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');

remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');
remove_action('wp_head', 'wp_shortlink_wp_head');

function basetheme_langswitcher($type = 'code')
{
    if (function_exists('icl_get_languages')) {
        $languages = array_reverse(icl_get_languages('skip_missing=0&orderby=KEY&order=DIR'));
        if (1 < count($languages)) {
            foreach ($languages as $key => $l) {
                $display;
                switch ($type) {
                    case 'flag':
                        $display = '<img src="' . $l['country_flag_url'] . '" />';
                        break;

                    case 'code':
                        $display = $l['language_code'];
                        break;

                    case 'native_name':
                        $display = $l['native_name'];
                        break;

                    case 'translated_name':
                        $display = $l['translated_name'];
                        break;
                    
                    default:
                        $display = $l['native_name'];
                        break;
                }
                $langs[$key] = '<li class="current_language"><a href="' . $l['url'] . '">' . $display . '</a></li>';
                if ($l['active'] != 1) {
                    $langs[$key] = '<li><a href="' . $l['url'] . '">' . $display . '</a></li>';
                }
            }
            echo apply_filters('basetheme_langswitcher', sprintf('<ul class="wpml">%s</ul>', join(' ', $langs)));
        }
    }
}

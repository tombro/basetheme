<?php
/**
 * Class for adding an custom options
 */
class Add_Settings_Fields
{

    /**
     * Class constructor
     */
    public function __construct()
    {
        add_action('admin_init', array( $this , 'registerFields' ));
    }

    /**
     * Add new fields to wp-admin/options-reading.php page
     */
    public function registerFields()
    {
        register_setting('reading', 'page_for_search', 'esc_attr');
        add_settings_field(
            'extra_blog_desc_id',
            '<label for="extra_blog_desc_id">' . __('Page for search', 'basetheme') . '</label>',
            array( $this, 'searchPageHtml' ),
            'reading'
        );
    }

    /**
     * HTML for extra settings
     */
    public function searchPageHtml()
    {
        wp_dropdown_pages(array( 'name' => 'page_for_search', 'echo' => 1, 'show_option_none' => __('&mdash; Select &mdash;'), 'option_none_value' => '0', 'selected' => get_option('page_for_search') )) ;
        printf('<p class="description">%s</p>', __('Use this option if your theme is using a custom search page.', 'basetheme'));
    }
}
new Add_Settings_Fields();

class SocialSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action('admin_menu', array( $this, 'addPluginPage' ));
        add_action('admin_init', array( $this, 'pageInit' ));
    }

    /**
     * Add options page
     */
    public function addPluginPage()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin',
            __('Social', 'basetheme'),
            'manage_options',
            'social-settings',
            array( $this, 'createAdminPage' )
        );
    }

    /**
     * Options page callback
     */
    public function createAdminPage()
    {
        // Set class property
        $this->options = get_option('social'); ?>
        <div class="wrap">
            <h1><?php __('Social Media', 'basetheme') ?></h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields('social_option_group');
        do_settings_sections('social-settings');
        submit_button(); ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function pageInit()
    {
        $socialMedia = array(
            'facebook'      => 'Facebook',
            'twitter'       => 'Twitter',
            'instagram'     => 'Instagram',
            'pinterest'     => 'Pinterest',
            'linkedin'      => 'Linkedin',
            'google-plus'   => 'Google+'
        );
        register_setting(
            'social_option_group', // Option group
            'social', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            __('Social Media URL\'s', 'basetheme'), // Title
            array( $this, 'printSectionInfo' ), // Callback
            'social-settings' // Page
        );
        foreach ($socialMedia as $slug => $name) {
            add_settings_field(
                $slug,
                $name,
                array( $this, 'socialCallback' ),
                'social-settings',
                'setting_section_id',
                array(
                    'slug' => $slug,
                    'name' => $name
                )
            );
        }
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input)
    {
        $newInput = array();

        foreach ($input as $key => $value) {
            $newInput[$key] = esc_url($input[$key]);
        }

        return $newInput;
    }

    /**
     * Print the Section text
     */
    public function printSectionInfo()
    {
        print 'Enter your social media url\'s below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function socialCallback($args)
    {
        printf(
            '<input type="text" id="title" name="social[' . $args['slug'] . ']" value="%s" />',
            isset($this->options[$args['slug']]) ? esc_attr($this->options[$args['slug']]) : ''
        );
    }
}

if (is_admin()) {
    $social_settings_page = new SocialSettingsPage();
}

?>
<?php
/**
 * Basetheme: Customizer
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function basetheme_customize_register($wp_customize)
{

    // Load plugin.php for is_plugin_active function
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
    
    $wp_customize->get_setting('blogname')->transport          = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport   = 'postMessage';

    $wp_customize->selective_refresh->add_partial('blogname', array(
        'selector' => '.site-title a',
        'render_callback' => 'basetheme_customize_partial_blogname',
    ));
    $wp_customize->selective_refresh->add_partial('blogdescription', array(
        'selector' => '.site-description',
        'render_callback' => 'basetheme_customize_partial_blogdescription',
    ));

    //Section for GA
    $wp_customize->add_section(
            'basetheme_analytics',
            array(
                'title' => __('Google Analytics', 'basetheme'),
                'description' => __('Add Google Analytics functionality to each page of your website.', 'basetheme'),
                'priority' => 200
            )
        );

    //Add Setting for GA
    $wp_customize->add_setting(
        'basetheme_ga',
        array(
            'sanitize_callback' => 'basetheme_sanitize_text'
        )
    );

    //Add Control for GA
    $wp_customize->add_control(
        'basetheme_ga',
        array(
            'type' => 'text',
            'label' => __('Google Analytics Tracking ID', 'basetheme'),
            'section' => 'basetheme_analytics',
        )
    );

    //Section for Developer
    $wp_customize->add_section(
            'basetheme_development',
            array(
                'title' => __('Development', 'basetheme'),
                'description' => __('Select which menu items should be visible in the admin menu.', 'basetheme'),
                'priority' => 200
            )
        );

    if (is_plugin_active('advanced-custom-fields-pro/acf.php')) {
        //Activate ACF Pro menu item
        $wp_customize->add_setting(
            'basetheme_acf_pro',
            array(
                'sanitize_callback' => 'basetheme_sanitize_checkbox'
            )
        );
        $wp_customize->add_control(
            'basetheme_acf_pro',
            array(
                'type' => 'checkbox',
                'label' => __('ACF Pro', 'basetheme'),
                'section' => 'basetheme_development',
            )
        );
    }

    if (is_plugin_active('w3-total-cache/w3-total-cache.php')) {
        //Activate W3TC menu item
        $wp_customize->add_setting(
            'basetheme_w3tc',
            array(
                'sanitize_callback' => 'basetheme_sanitize_checkbox'
            )
        );
        $wp_customize->add_control(
            'basetheme_w3tc',
            array(
                'type' => 'checkbox',
                'label' => __('W3 Total Cache', 'basetheme'),
                'section' => 'basetheme_development',
            )
        );
    }
}
add_action('customize_register', 'basetheme_customize_register');

function basetheme_sanitize_checkbox($input)
{
    if ($input == 1) {
        return 1;
    }
    return '';
}
function basetheme_sanitize_text($input)
{
    return wp_kses_post(force_balance_tags($input));
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @since Basetheme 1.0
 * @see basetheme_customize_register()
 *
 * @return void
 */
function basetheme_customize_partial_blogname()
{
    bloginfo('name');
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since Basetheme 1.0
 * @see basetheme_customize_register()
 *
 * @return void
 */
function basetheme_customize_partial_blogdescription()
{
    bloginfo('description');
}

/**
 * Return whether we're previewing the front page and it's a static page.
 */
function basetheme_is_static_front_page()
{
    return (is_front_page() && ! is_home());
}

/**
 * Bind JS handlers to instantly live-preview changes.
 */
function basetheme_customize_preview_js()
{
    wp_enqueue_script('basetheme-customize-preview', get_theme_file_uri('/assets/js/customize-preview.js'), array( 'customize-preview' ), '1.0', true);
}
add_action('customize_preview_init', 'basetheme_customize_preview_js');

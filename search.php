<?php
get_header(); ?>
<div class="container-fluid">
	<div class="row">
		<header class="page-header col-12">
			<?php if (have_posts()) : ?>
				<h1 class="page-title"><?php printf(__('Search Results for: %s', 'basetheme'), '<span>' . get_search_query() . '</span>'); ?></h1>
			<?php else : ?>
				<h1 class="page-title"><?php _e('Nothing Found', 'basetheme'); ?></h1>
			<?php endif; ?>
		</header><!-- .page-header -->
	</div>
	<div class="row">
		<div id="primary" class="content-area col-sm-8">
			<main id="main" class="site-main" role="main">

			<?php
            if (have_posts()) :
                while (have_posts()) : the_post();

                    get_template_part('template-parts/post/content', 'excerpt');

                endwhile;

                the_posts_pagination(array(
                    'prev_text' => '<i class="fa fa-arrow-left" aria-hidden="true"></i><span class="screen-reader-text">' . __('Previous page', 'basetheme') . '</span>',
                    'next_text' => '<span class="screen-reader-text">' . __('Next page', 'basetheme') . '</span><i class="fa fa-arrow-right" aria-hidden="true"></i>',
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Page', 'basetheme') . ' </span>',
                ));

            else : ?>

				<p><?php _e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'basetheme'); ?></p>
				<?php
                    get_search_form();

            endif;
            ?>

			</main><!-- #main -->
		</div><!-- #primary -->
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer();

<?php
/**
 * The template for displaying 404 pages (not found)
 */

get_header(); ?>

<div class="container-fluid">
	<div class="row">
		<section class="col-12 error-404 not-found">
			<header class="page-header">
				<h1 class="page-title"><?php _e('Oops! That page can&rsquo;t be found.', 'basetheme'); ?></h1>
			</header><!-- .page-header -->
			<div class="page-content">
				<p><?php _e('It looks like nothing was found at this location. Maybe try a search?', 'basetheme'); ?></p>

				<?php get_search_form(); ?>

			</div><!-- .page-content -->
		</section><!-- .error-404 -->
	</div>
</div><!-- #primary -->

<?php get_footer();

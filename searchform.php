<?php 
$search_url 	= esc_url(home_url('/'));
$unique_id 		= esc_attr(uniqid('search-form-'));
$label 			= _x('Search for:', 'label', 'basetheme');
$placeholder 	= esc_attr_x('Search &hellip;', 'placeholder', 'basetheme');
$query 			= get_search_query();
?>
<form role="search" method="get" class="search-form form-inline" action="<?php echo $search_url; ?>">
	<div class="input-group">
		<label for="<?php echo $unique_id; ?>">
			<span class="screen-reader-text"><?php echo $label; ?></span>
		</label>
		<input class="form-control py-2 border-right-0 border" type="search" id="<?php echo $unique_id; ?>" placeholder="<?php echo $placeholder; ?>" value="<?php echo $query; ?>" name="s">
		<span class="input-group-append">
			<button class="btn btn-outline-secondary border-left-0 border" type="submit">
				<i class="fa fa-search"></i>
			</button>
		</span>
	</div>
</form>